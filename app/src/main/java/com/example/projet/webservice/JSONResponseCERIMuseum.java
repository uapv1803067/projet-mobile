package com.example.projet.webservice;
//cf HANDLERMATCH
/*
import android.util.JsonReader;
import android.util.JsonToken;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseCERIMuseum
{
    private static final String TAG = JSONResponseCERIMuseum.class.getSimpleName();
    private Item ceri;

    public JSONResponseCERIMuseum(Item ceri)
    {
        this.ceri = ceri;
    }
    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     *//*
    public void readJsonStream(InputStream response) throws IOException
    {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try
        {
            readItem(reader);
        } finally {
            reader.close();
        }
    }

    public void readItem(JsonReader reader) throws IOException
    {
        reader.beginObject();
        while (reader.hasNext())
        {
            String name = reader.nextName();
            if (name.equals("results"))
            {
                readArrayItem(reader);
            }
            else
            {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    private void readArrayItem(JsonReader reader) throws IOException
    {
        reader.beginArray();
        JsonToken check;
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext())
            {
                check =reader.peek();
                if(check!=JsonToken.NULL)
                {
                    String name = reader.nextName();
                    if (nb == 0)
                    {
                        if (name.equals("ids"))
                        {
                            ceri.setId(reader.nextLong());
                        }
                        else if (name.equals("categories"))
                        {
                            ceri.setCategories(reader.nextString());
                        }
                        else if (name.equals("demos"))
                        {
                            ceri.setDemos(reader.nextString());
                        }
                        else if (name.equals("catalog"))
                        {
                            ceri.setCatalog(reader.nextString());
                        }/*
                        else if (name.equals("items"))
                        {
                            ceri.setItems(reader.nextString());
                        }
                        else if (name.equals("images"))
                        {
                            ceri.setImages(reader.nextString());
                        }*//*
                        else
                        {
                            reader.skipValue();
                        }
                    }
                    else
                    {
                        reader.skipValue();
                    }
                }
                else
                {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}
*/