package com.example.projet.webservice;
// cf Match TP3
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Item
{
    //public static final String TAG = Item.class.getSimpleName();
    private String id;
    public String name;
    private ArrayList<String> categories;
    private ArrayList<String> technicalDetails;
    private String demos;
    private String catalog;
    private String brand;
    private int items;
    private Drawable thumbnail;
    private int images;
    private int year;
    public int[] timeFrame;
    //private List<Bitmap> bitmaps;
    //private Map<String, Bitmap> bitPics;

    // parler des getter / setter et de la sécu liée à ça par rapport
    // a musee.name et musee getname
    // final = ne peut plus etre modifiée ou que ce soit

    public Item(Item item)
    {
        this.id = item.id;
        this.categories = new ArrayList<>(); for(String str : item.categories)this.categories.add(str);
        this.brand = item.brand;
        this.demos = item.demos;
        this.catalog = item.catalog;
        this.timeFrame = item.timeFrame;
        this.thumbnail = item.thumbnail;
        this.year = item.year;
        //this.items = items;
        //this.images = images;
    }

    public Item(JSONObject jsonObject, String id)
    {
        try
        {
            this.id = id;
            if(jsonObject.has("name")) name = jsonObject.getString("name");
            if(jsonObject.has("year")) year = jsonObject.getInt("year");
            if(jsonObject.has("brand")) brand = jsonObject.getString("brand");
            if(jsonObject.has("categories"))
            {
                categories = new ArrayList<>();
                JSONArray categoriesItemJSON= jsonObject.getJSONArray("categories");
                for (int i = 0; i<categoriesItemJSON.length(); i++)
                    categories.add(categoriesItemJSON.getString(i));
            }
            if(jsonObject.has("timeFrame"))
            {
                JSONArray timeframeJson = jsonObject.getJSONArray("timeFrame");
                timeFrame = new int[timeframeJson.length()];
                for (int i = 0; i<timeframeJson.length(); i++)
                    timeFrame[i] = timeframeJson.getInt(i);
            }
        }
        catch (Exception e){e.printStackTrace();}
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
/*
    public String getCategories() {
        return categories;
        //getlabel
    }

    public void setCategories(String categories) {
        this.categories = categories;
        //setlabel
    }

    public String getDemos() {
        return demos;
        //hometeam
    }

    public void setDemos(String demos) {
        this.demos = demos;
    }

    public String getCatalog() {
        return catalog;
        //awayteam
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(categories);
        dest.writeString(demos);
        dest.writeString(catalog);
        dest.writeString(String.valueOf(items));
        dest.writeString(String.valueOf(images));
    }
*/
    public String toString()
    {
        return categories + " : "+items+"-"+images;
    }

    public Drawable getThumbnail()
    {
        return thumbnail;
    }

    public void setThumbnail(Drawable drawable)
    {
        thumbnail = drawable;
    }

}
