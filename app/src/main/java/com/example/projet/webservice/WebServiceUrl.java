package com.example.projet.webservice;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class WebServiceUrl
{
    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }
    /* ********************************************************************** */
    // Find IDS
    private static final String SEARCH_IDS = "ids";

    // Build URL to get information for a specific id
    public static URL buildSearchIds() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_IDS);

        URL url = new URL(builder.build().toString());
        return url;
    }
    /* ********************************************************************** */
    // Build URL to get informations from categories
    private static final String MUSEUM_CATEGORIES = "categories";

    public static URL buildSearchLastEvents() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(MUSEUM_CATEGORIES);

        URL url = new URL(builder.build().toString());
        return url;
    }
    /* ********************************************************************** */
    // Find demos
    private static final String SEARCH_DEMOS = "demos";

    // Build URL to get information for a specific demos
    public static URL buildSearchDemos() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_DEMOS);

        URL url = new URL(builder.build().toString());
        return url;
    }
    /* ********************************************************************** */
    // Find catalog
    private static final String SEARCH_CATALOG = "catalog";

    // Build URL to get information for a specific catalog
    public static URL buildSearchCatalog() throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);

        URL url = new URL(builder.build().toString());
        return url;
    }
    /* ********************************************************************** */
    // Find items
    private static final String SEARCH_ITEMS = "items";

    // Build URL to get information for a specific item id
    public static URL buildSearchItems(String itemsId) throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(String.valueOf(itemsId));

        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_THUMBNAIL = "thumbnail";

    // find thumbnail by ID
    public static URL buildSearchThumbnail(String itemsIdThumbnail) throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(String.valueOf(itemsIdThumbnail))
                .appendPath(SEARCH_THUMBNAIL);

        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String SEARCH_HDIMAGES = "images";
    // find HD images by ID
    // image's item ID = itemId
    public static URL buildSearchImagesId(String itemsId, String imagesId) throws MalformedURLException
    {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_ITEMS)
                .appendPath(String.valueOf(itemsId))
                .appendPath(SEARCH_HDIMAGES)
                .appendPath(String.valueOf(imagesId));

        URL url = new URL(builder.build().toString());
        return url;
    }
    /* ********************************************************************** */
    public String getContentOfUrl(URL link)
    {
        try
        {
            HttpsURLConnection connect = (HttpsURLConnection) link.openConnection();
            InputStream inputStream = connect.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();
            inputStream.close();
            connect.disconnect();
            return sb.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }
}
// Suite à la modification du type de variables le String.ValueOf est inutile.
// Pensez à le retirer plus tard, devrait fonctionner avec tout de même.
