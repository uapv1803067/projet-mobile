package com.example.projet;

import com.example.projet.webservice.Item;
// cf TP3 TeamMatchHandler
import java.util.ArrayList;

public class MuseumDataHandler
{
    protected static final String SITE = "https://demo-lia.univ-avignon.fr/cerimuseum/";
    MainActivity activity;

    static ArrayList<String> ids;
    static ArrayList<Item> items;
    static ArrayList<String> categories;
    
    public MuseumDataHandler(MainActivity activity)
    {
        this.activity = activity;
        requestItem("categories");
        requestItem("ids");
    }

    private void requestItem(String mainPage)
    {
        final String currentPage = mainPage;
        /* TODO here maybe see for another AsyncTask ? */
    }
}
