package com.example.projet;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.projet.webservice.Item;

import java.util.ArrayList;

public class Museum extends BaseAdapter
{
    private Activity activity;
    private ArrayList<String> stringList;
    private static LayoutInflater inflater;
    private ArrayList<Item> itemList;

    public static class ViewHolder
    {
        public TextView name;
        public TextView year;
        public ImageView image;
        public TextView description;
    }

    public Museum(Activity activity, ArrayList<String> stringList)
    {
        this.activity = activity;
        this.stringList = stringList;
        Museum.inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return stringList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View view =convertView;
        ViewHolder holder;

        if(convertView == null)
        {

            view = inflater.inflate(R.layout.museum, null);

            holder=new ViewHolder();
            holder.name=(TextView)view.findViewById(R.id.nameMuseum);
            holder.description=(TextView)view.findViewById(R.id.descriptionMuseum);
            holder.image = (ImageView)view.findViewById(R.id.imageMuseum);

            view.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) view.getTag();
        }
        Item item = itemList.get(position);
        holder.name.setText(item.name);
       //holder.name.setText("texte 1");
        //holder.description.setText("test 1");
        //    holder.image.setImageResource();

        return view;
    }
}
