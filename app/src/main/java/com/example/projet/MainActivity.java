package com.example.projet;

import android.os.Bundle;
import com.example.projet.webservice.WebServiceUrl;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity
{
    protected ListView listView;
    protected MuseumDataHandler littleCERIMuseum;
    protected WebServiceUrl webService = new WebServiceUrl();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       // littleCERIMuseum = new MuseumDataHandler(this);
        listView = findViewById(R.id.itemListView);

        URL urlIds = null;
        try {
            urlIds = webService.buildSearchIds();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JSONObject jsonIds = null;
        try {
            jsonIds = new JSONObject(webService.getContentOfUrl(urlIds));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //plutot a mettre a part, dans une fonction specifique au traitement des Ids
        Iterator<String> index = jsonIds.keys();
        String key = null;
        ArrayList<String> ids = null;
        int i=0;
        while(index.hasNext())
        {
            key = index.next();
            try {
                if (jsonIds.get(key) instanceof JSONObject) {
                    ids.add(i, (String) jsonIds.get(key));
                    i++;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // et la on fait le updateList en lui donnant la liste des id (qui correspond au nb de container a créer)
        updateList(ids);

        //updateList();
    }

    private void updateList(ArrayList<String> items)
    {
        final Museum adapter = new Museum(MainActivity.this, items);
        listView.setAdapter((ListAdapter) adapter);
        //String[] values = new String[] {"Test 1","Test 2"}; // Test d'affichage temp

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //return true;
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView =
                (SearchView) searchItem.getActionView();

        // Configure the search info and add any event listeners...

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
       /* int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }
*/
        switch (item.getItemId())
        {
            case R.id.menu:
                // User chose the "settings" item, show the app settings UI...
                return true;

            case R.id.action_search:
                // User chose the "search" action
                //please do smth
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
}